﻿using System;
using System.Collections.Generic;
using System.Text;
using FolderCrawler.Entities;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace FolderCrawler
{
    public class ApplicationViewModel : INotifyPropertyChanged
    {
        private PersonalCard selectedPerson;

        private ApplicationSettings settings;

        private ObservableCollection<PersonalCard> persons;

        public ObservableCollection<PersonalCard> Persons
        {
            get {  return persons;  }
            set
            {
                persons = value;
                OnPropertyChanged("Persons");
            }
        }

        public ApplicationViewModel()
        {

        }
        public ApplicationSettings Settings
        {
            get { return settings; }
            set
            {
                settings = value;
                OnPropertyChanged("Settings");
            }
        }
        public PersonalCard SelectedPerson
        {
            get { return selectedPerson; }
            set
            {
                selectedPerson = value;
                OnPropertyChanged("SelectedPerson");
            }
        }
        public string PersonalCardPath { get; } = @"PersonalCards";

        public string SettingsPath { get; } = @"Settings";

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
