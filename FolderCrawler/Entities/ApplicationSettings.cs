﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.IO;

namespace FolderCrawler.Entities
{
    public class ApplicationSettings : INotifyPropertyChanged
    {
        private double criticalTemperature = 37.0d;
        private int maxElemCount = 1000;
        public string FilePath { get; } = @"Settings\SettingsFile.txt";
        public ApplicationSettings()
        {

        }
        public ApplicationSettings(double criticalTemperature, int maxElemCount, string filePath)
        {
            this.criticalTemperature = criticalTemperature;
            this.maxElemCount = maxElemCount;
            FilePath = filePath;
        }
        public double CriticalTemperature
        {
            get
            {
                return criticalTemperature;
            }
            set
            {
                criticalTemperature = value;
                OnPropertyChanged("CriticalTemperature");
            }
        }
        public int MaxElemCount
        {
            get
            {
                return maxElemCount;
            }
            set
            {
                maxElemCount = value;
                OnPropertyChanged("MaxElemCount");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}

