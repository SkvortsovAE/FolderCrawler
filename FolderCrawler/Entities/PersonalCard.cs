﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace FolderCrawler.Entities
{
    public class PersonalCard : INotifyPropertyChanged
    {
        private string name;
        private string surname;
        private int age;
        private string photoPath;
        private DateTime addInfo;
        private double temperature;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public string Surname
        {
            get
            {
                return surname;
            }
            set
            {
                surname = value;
                OnPropertyChanged("Surname");
            }
        }
        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                age = value;
                OnPropertyChanged("Age");
            }
        }
        public string PhotoPath
        {
            get
            {
                return photoPath;
            }
            set
            {
                photoPath = value;
                OnPropertyChanged("PhotoPath");
            }
        }
        public DateTime AddInfo
        {
            get
            {
                return addInfo;
            }
            set
            {
                addInfo = value;
                OnPropertyChanged("AddInfo");
            }
        }
        public double Temperature
        {
            get
            {
                return temperature;
            }
            set
            {
                temperature = value;
                OnPropertyChanged("Temperature");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
