﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using FolderCrawler.Entities;
using FolderCrawler.Utilities;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;

namespace FolderCrawler
{
    public partial class SettingsWindow : Window
    {
        public ApplicationViewModel context;
        public SettingsWindow(ApplicationViewModel context)
        {
            InitializeComponent();
            this.context = context;
            DataContext = context;
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            CriticalTemepatureText.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            MaxElemCountText.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            DataUtilities.SaveSettingsToFile(context.Settings);
            RefreshMainView();
            Close();
        }
        public void RefreshMainView() //Обновляем список элементов в ListBox связанного окна MainWindow
        {
            if (context.Persons.Count > context.Settings.MaxElemCount)
            {
                while (context.Persons.Count > context.Settings.MaxElemCount)
                {
                    context.Persons.Remove(context.Persons.Aggregate((x, y) => x.AddInfo < y.AddInfo ? x : y));
                }
            }
            else
            {
                context.Persons = DataUtilities.ConvertFilesToPersonalCards(Directory.EnumerateFiles(context.PersonalCardPath).Where(
                    x => Path.GetExtension(x) == ".jpg").ToList(), context.Settings.MaxElemCount);
            }
            context.Persons = new ObservableCollection<PersonalCard>(context.Persons);
        }
    }
}
