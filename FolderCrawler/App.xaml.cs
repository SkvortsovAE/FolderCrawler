﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;

namespace FolderCrawler
{
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e) //устанавливаем культуру
        {
            var customCulture = CultureInfo.GetCultureInfo("ru-RU").Clone() as CultureInfo;
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            customCulture.NumberFormat.CurrencyDecimalSeparator = ".";
            Thread.CurrentThread.CurrentCulture = customCulture;
            Thread.CurrentThread.CurrentUICulture = customCulture;
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(
                        XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));
        }
    }
}