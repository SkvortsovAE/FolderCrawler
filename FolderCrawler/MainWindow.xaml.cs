﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.IO;
using FolderCrawler.Entities;
using FolderCrawler.Utilities;

namespace FolderCrawler
{
    public partial class MainWindow : Window
    {
        private FileSystemWatcher watcher = new FileSystemWatcher();
        public ApplicationViewModel context = new ApplicationViewModel();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = context;
        }
        private void Window_Loaded(object sender, EventArgs e) //Стартовая подготовка
        {
            if (!Directory.Exists(context.SettingsPath)) //Создание директории для файла настроек
            {
                Directory.CreateDirectory(context.SettingsPath);
            }
            string settingsFilePath = Path.GetFullPath(context.SettingsPath + @"\SettingsFile.txt");
            if (!File.Exists(settingsFilePath)) //Создание файла настроек или чтение из него при наличии
            {
                DataUtilities.SaveSettingsToFile(new ApplicationSettings(37.0, 1000, settingsFilePath));
            }
            else
            {
                context.Settings = DataUtilities.GetSettingsFromFile(settingsFilePath);
            }
            if (!Directory.Exists(context.PersonalCardPath)) //Создание директории для хранения карточек или чтение из нее при наличии
            {
                Directory.CreateDirectory(context.PersonalCardPath);
            }
            else
            {
                context.Persons = DataUtilities.ConvertFilesToPersonalCards(Directory.EnumerateFiles(context.PersonalCardPath).Where(
                    x => Path.GetExtension(x) == ".jpg").ToList(), context.Settings.MaxElemCount);
            }
            //Конфигурация наблюдателя папки
            watcher.Path = context.PersonalCardPath; 

            watcher.NotifyFilter = NotifyFilters.LastAccess
                                 | NotifyFilters.LastWrite
                                 | NotifyFilters.FileName
                                 | NotifyFilters.DirectoryName;

            watcher.Filter = "*.jpg";

            watcher.Created += OnCreated;

            watcher.EnableRaisingEvents = true;
        }

        private void OnCreated(object source, FileSystemEventArgs e) //Конфигурация делегата для наблюдателя
        {
            FolderCrawler.App.Current.Dispatcher.Invoke(delegate
            {
                context.Persons.Insert(0, DataUtilities.ConvertFileToPersonalCard(e.FullPath)); //Добавляем элементв начало списка, если список переполнен удаляем самый старый элемент
                if (context.Persons.Count >= context.Settings.MaxElemCount)
                {
                    context.Persons.Remove(context.Persons.Aggregate((x, y) => x.AddInfo < y.AddInfo ? x : y));
                }
            });
        }
        private void Open_Settings_Window(object sender, RoutedEventArgs e)
        {
            SettingsWindow settingsWindow = new SettingsWindow(context);
            settingsWindow.ShowDialog();
        }
    }
}
