﻿using System.Windows;
using System.Windows.Controls;
using FolderCrawler.Entities;
using FolderCrawler;


namespace FolderCrawler.Selectors
{
    public class CardItemDataTemplateSelector : DataTemplateSelector //Селектор для выбора нужного шаблона
    {
        public override DataTemplate
            SelectTemplate(object item, DependencyObject container)
        {
            if (item != null && item is PersonalCard)
            {
                PersonalCard cardItem = item as PersonalCard;
                Window window = Application.Current.MainWindow;
                if(cardItem.Temperature >= (window.DataContext as ApplicationViewModel).Settings.CriticalTemperature)
                {
                    return window.FindResource("Warning") as DataTemplate;
                }
                else
                {
                    return window.FindResource("Normal") as DataTemplate;
                }
            }

            return null;
        }
    }
}
