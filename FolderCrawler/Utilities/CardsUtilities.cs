﻿using System;
using System.Collections.Generic;
using System.Text;
using FolderCrawler.Entities;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace FolderCrawler.Utilities
{
    public static partial class DataUtilities
    {
        public static PersonalCard ConvertFileToPersonalCard(string filePath)
        {
            string[] rows = filePath.Substring(filePath.LastIndexOf(@"\")+1).Split('_');
            try
            {
                return new PersonalCard
                {
                    Name = rows[0],
                    Surname = rows[1],
                    Age = Convert.ToInt32(rows[2]),
                    PhotoPath = Path.GetFullPath(filePath),
                    AddInfo = Directory.GetCreationTime(filePath),
                    Temperature = Convert.ToDouble(rows[3])
                };
            }
            catch
            {
                return null;
            }
        }
        public static ObservableCollection<PersonalCard> ConvertFilesToPersonalCards(List<string> filePaths, int maxElemCount)
        {
            ObservableCollection<PersonalCard> result = new ObservableCollection<PersonalCard>();
            int i = 0;
            foreach(string path in filePaths)
            {
                result.Add(ConvertFileToPersonalCard(path));
                if (i>=maxElemCount)
                {
                    result.Remove(result.Aggregate((x, y) => x.AddInfo < y.AddInfo ? x : y));
                }
                i++;
            }
            return result;
        }
    }
}
