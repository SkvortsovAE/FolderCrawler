﻿using System;
using System.Collections.Generic;
using System.Text;
using FolderCrawler.Entities;
using System.IO;

namespace FolderCrawler.Utilities
{
    public static partial class DataUtilities
    {
        public static ApplicationSettings GetSettingsFromFile(string filePath)
        {
            var rows = File.ReadAllLines(filePath);
            try
            {
                return new ApplicationSettings(Convert.ToDouble(rows[0].Split(":")[1]), Convert.ToInt32(rows[1].Split(":")[1]), filePath);
            }
            catch
            {
                return new ApplicationSettings();
            }
        }
        public static void SaveSettingsToFile(ApplicationSettings applicationSettings)
        {
            try
            {
                File.WriteAllText(applicationSettings.FilePath, "CriticalTemperature:" + applicationSettings.CriticalTemperature.ToString() +
                        "\r\nMaxElemCount:" + applicationSettings.MaxElemCount.ToString());
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
